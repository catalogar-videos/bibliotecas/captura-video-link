package lib.captura.video.link;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.storage.Metadata;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class VideoSourceSkeleton extends VideoSourceLinkCapture {

    VideoSourceSkeleton(final String directory, final Channel channel, final IHttpClient iHttpClient) {
        super(channel, iHttpClient);
        this.directory = directory;
    }

    private final String directory;

    @Override
    public Collection<String> requiredParameters() {
        return List.of("id", "name", "author", "createDate");
    }

    @Override
    protected Collection<Link> links(final Map<String, String> parameters) {
        return Stream.of
                        (
                                "https://videos.pexels.com/video-files/{}/{}-s1_426_240_25fps.mp4",
                                "https://videos.pexels.com/video-files/{}/{}-sd_426_240_25fps.mp4"
                        )
                .map(s ->
                        new Link(directory) {
                            @Override
                            public String url() {
                                return s.replace("{}", parameters.get("id"));
                            }

                            @Override
                            public String fileExtension() {
                                return "mp4";
                            }
                        }
                )
                .collect(Collectors.toSet());
    }

    @Override
    protected Metadata buildMetadata(final Map<String, String> parameters) {

        return new Metadata() {
            @Override
            public String name() {
                return parameters.get("name");
            }

            @Override
            public String author() {
                return parameters.get("author");
            }

            @Override
            public Collection<String> tags() {
                return Arrays.stream(parameters.get("tags").split(",")).toList();
            }

            @Override
            public Optional<LocalDate> createDate() {
                return Optional.of(LocalDate.parse(parameters.get("createDate")));
            }

        };

    }

}
