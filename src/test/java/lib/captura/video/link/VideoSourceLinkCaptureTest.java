package lib.captura.video.link;

import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.Information;
import lib.captura.video.core.channel.Message;
import lib.captura.video.core.source.Video;
import lib.captura.video.core.source.VideoNullObject;
import lib.captura.video.core.source.VideoSource;
import lib.captura.video.core.source.VideoSourceEntry;
import lib.captura.video.core.storage.Metadata;
import lib.captura.video.core.storage.Raw;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.http.HttpClient;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

class VideoSourceLinkCaptureTest {

    @ParameterizedTest
    @ValueSource(strings = {"/tmp"})
    void shouldCaptureVideo(final String directory) throws IOException {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final VideoSourceEntry videoSourceHttp = new VideoSourceSkeleton
                (
                        directory,
                        atomicReference::set,
                        HttpClient::newHttpClient
                );

        Assertions.assertEquals(videoSourceHttp.requiredParameters(), videoSourceHttp.requiredParameters());

        final Map<String, String> data = Map.of
                (
                        "id", "15921892",
                        "name", "a-black-and-white-cow-grazing-in-a-field",
                        "author", "ed1toR",
                        "tags", "white cow,cow,bovine",
                        "createDate", "2023-03-13"
                );

        final AtomicReference<Raw> rawAtomicReference = new AtomicReference<>();

        videoSourceHttp.video(data).write(rawAtomicReference::set);

        final Raw raw = rawAtomicReference.get();

        Assertions.assertNotNull(raw);

        final Path path = Path.of(raw.videoFilePath());

        final Metadata metadata = raw.metadata();

        Assertions.assertNotNull(metadata);

        final Message message = atomicReference.get();

        Assertions.assertEquals(Event.VIDEO_CAPTURED, message.event());

        final Optional<Information> optionalInformation = message.infos().stream().findFirst();

        Assertions.assertTrue(optionalInformation.isPresent());

        final Information information = optionalInformation.get();

        assertData(information, data, path, metadata);

        Files.delete(path);

    }

    @Test
    void whenLinksEmptyShouldReturnVideoNullObject() {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final VideoSource videoSource =
                new VideoSourceSkeleton("_ ->", atomicReference::set, HttpClient::newHttpClient) {
                    @Override
                    protected Collection<Link> links(final Map<String, String> parameters) {
                        return Collections.emptyList();
                    }
                };

        final Video video = videoSource.video(Map.of());

        final Message message = atomicReference.get();

        Assertions.assertNotNull(message);

        Assertions.assertEquals(Event.VIDEO_NOT_FOUND, message.event());

        Assertions.assertInstanceOf(VideoNullObject.class, video);

    }

    @Test
    void whenIOExceptionRaise() {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final String directory = "";

        final VideoSourceLinkCapture videoSourceLinkCapture = new VideoSourceSkeleton
                (
                        directory,
                        atomicReference::set,
                        () -> {
                            throw new IOException();
                        }
                ) {
            @Override
            protected Collection<Link> links(Map<String, String> parameters) {
                return List.of
                        (
                                new Link(directory) {
                                    @Override
                                    protected String url() {
                                        return "";
                                    }

                                    @Override
                                    protected String fileExtension() {
                                        return "";
                                    }
                                }
                        );
            }
        };

        Map<String, String> parameters = Map.of("id", "123");

        videoSourceLinkCapture.video(parameters);

        final Message message = atomicReference.get();

        Assertions.assertNotNull(message);

        Assertions.assertEquals(Event.VIDEO_SOURCE_UNAVAILABLE, message.event());

    }

    @Test
    void whenInterruptExceptionRaise() {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final String directory = "";

        final VideoSourceLinkCapture videoSourceLinkCapture = new VideoSourceSkeleton
                (
                        directory,
                        atomicReference::set,
                        () -> {
                            throw new InterruptedException();
                        }
                ) {
            @Override
            protected Collection<Link> links(Map<String, String> parameters) {
                return List.of
                        (
                                new Link(directory) {
                                    @Override
                                    protected String url() {
                                        return "";
                                    }

                                    @Override
                                    protected String fileExtension() {
                                        return "";
                                    }
                                }
                        );
            }
        };

        Map<String, String> parameters = Map.of("id", "123");

        videoSourceLinkCapture.video(parameters);

        final Message message = atomicReference.get();

        Assertions.assertNotNull(message);

        Assertions.assertEquals(Event.VIDEO_SOURCE_UNAVAILABLE, message.event());

    }

    private static void assertData
            (
                    final Information information,
                    final Map<String, String> data,
                    final Path path,
                    final Metadata metadata
            ) {

        Assertions.assertEquals("Video Source HTTP", information.key());

        Assertions.assertNotNull(information.value());

        Assertions.assertTrue(Files.exists(path));

        Assertions.assertEquals(data.get("name"), metadata.name());

        Assertions.assertEquals(data.get("author"), metadata.author());

        Assertions.assertTrue
                (
                        metadata
                                .tags()
                                .containsAll
                                        (
                                                Arrays
                                                        .stream(data.get("tags").split(","))
                                                        .toList()
                                        )
                );

        final Optional<LocalDate> optionalLocalDate = metadata.createDate();

        Assertions.assertTrue(optionalLocalDate.isPresent());

        Assertions.assertTrue(optionalLocalDate.get().isEqual(LocalDate.parse(data.get("createDate"))));

    }

}