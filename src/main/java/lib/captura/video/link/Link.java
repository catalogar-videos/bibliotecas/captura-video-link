package lib.captura.video.link;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

/**
 * The Link class represents a link to a file that can be retrieved using an HTTP request.
 * It provides methods to retrieve the file from the specified URL.
 */
public abstract class Link {

    protected Link(final String directory) {
        fileName = UUID.randomUUID().toString();
        this.directory = directory;
    }

    private final String directory;

    private final String fileName;

    /**
     * Retrieves the file from the specified URL using an HTTP GET request.
     *
     * @return a Response object containing the status code and the path of the downloaded file
     * @throws IOException          if an I/O error occurs
     * @throws InterruptedException if the operation is interrupted
     */
    final Response get(final IHttpClient iHttpClient) throws IOException, InterruptedException {

        try (final HttpClient httpClient = iHttpClient.newHttpClient()) {

            final HttpRequest httpRequest =
                    HttpRequest.newBuilder()
                            .uri(URI.create(url()))
                            .GET()
                            .build();

            final Path filePath = Path.of(directory, String.format("%s.%s", fileName, fileExtension()));

            final HttpResponse<Path> response = httpClient
                    .send
                            (
                                    httpRequest,
                                    HttpResponse.BodyHandlers.ofFile(filePath)
                            );

            if (response.statusCode() != 200) Files.delete(filePath);

            return new Response() {

                @Override
                public int statusCode() {
                    return response.statusCode();
                }

                @Override
                public String videoFilePath() {
                    return filePath.toString();
                }

            };
        }

    }

    /**
     * Returns the URL of the path file.
     *
     * @return the URL of the path file
     */
    protected abstract String url();

    /**
     * Retrieves the file extension of the path file.
     *
     * @return the file extension of the path file
     */
    protected abstract String fileExtension();

}
