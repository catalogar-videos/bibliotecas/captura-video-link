package lib.captura.video.link;

import java.io.IOException;
import java.net.http.HttpClient;

/**
 * This interface represents an HTTP client that can be used to send HTTP requests.
 * Implementations of this interface need to provide a method to create a new instance of the HttpClient.
 */
public interface IHttpClient {

    /**
     * Creates a new instance of HttpClient.
     *
     * @return a new instance of HttpClient
     * @throws IOException          if an I/O error occurs
     * @throws InterruptedException if the operation is interrupted
     */
    HttpClient newHttpClient() throws IOException, InterruptedException;

}
