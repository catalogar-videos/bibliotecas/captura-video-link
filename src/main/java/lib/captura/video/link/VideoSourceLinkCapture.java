package lib.captura.video.link;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.InformationBasic;
import lib.captura.video.core.channel.MessageBuilder;
import lib.captura.video.core.source.Video;
import lib.captura.video.core.source.VideoNullObject;
import lib.captura.video.core.source.VideoSourceEntry;
import lib.captura.video.core.storage.Metadata;
import lib.captura.video.core.storage.Raw;

import java.io.IOException;
import java.util.*;


/**
 * The VideoSourceLinkCapture class is an abstract class that represents a video source capture using a link.
 * It implements the VideoSourceEntry interface.
 */
public abstract class VideoSourceLinkCapture implements VideoSourceEntry {

    protected VideoSourceLinkCapture(final Channel channel, final IHttpClient iHttpClient) {
        this.channel = channel;
        this.iHttpClient = iHttpClient;
    }

    private final Channel channel;

    private final IHttpClient iHttpClient;

    /**
     * Retrieves a video based on the given parameters.
     *
     * @param parameters the parameters used to retrieve the video
     * @return the captured video
     */
    @Override
    public final Video video(final Map<String, String> parameters) {

        final Iterator<Link> iterator = links(parameters).iterator();

        while (iterator.hasNext()) {

            try {

                final Link link = iterator.next();

                final Response response = link.get(iHttpClient);

                if (response.statusCode() != 200) continue;

                send(response.videoFilePath(), Event.VIDEO_CAPTURED);

                return storage -> storage.write
                        (
                                new Raw() {

                                    @Override
                                    public Metadata metadata() {
                                        return buildMetadata(parameters);
                                    }

                                    @Override
                                    public String videoFilePath() {
                                        return response.videoFilePath();
                                    }

                                }
                        );

            } catch (final InterruptedException e) {
                send(e.toString(), Event.VIDEO_SOURCE_UNAVAILABLE);
                Thread.currentThread().interrupt();
                return new VideoNullObject();
            } catch (IOException e) {
                send(e.toString(), Event.VIDEO_SOURCE_UNAVAILABLE);
                return new VideoNullObject();
            }

        }

        send("Video Source Link", Event.VIDEO_NOT_FOUND);

        return new VideoNullObject();

    }

    /**
     * Sends a message with the given value and event using the channel.
     *
     * @param value the value to be sent
     * @param event the event associated with the value
     */
    private void send(final String value, final Event event) {

        channel.send
                (
                        MessageBuilder
                                .builder()
                                .withEvent(event)
                                .addInformation
                                        (
                                                new InformationBasic
                                                        (
                                                                "Video Source HTTP",
                                                                value
                                                        )
                                        )
                                .build()
                );
    }

    /**
     * Retrieves a collection of PathFile objects based on the given parameters.
     *
     * @param parameters the parameters used to retrieve the paths
     * @return a collection of PathFile objects
     */
    protected abstract Collection<Link> links(final Map<String, String> parameters);

    /**
     * Builds the metadata for the video source capture based on the given parameters.
     *
     * @param parameters the parameters to build the metadata from
     * @return the metadata for the video source capture
     */
    protected abstract Metadata buildMetadata(final Map<String, String> parameters);

}
