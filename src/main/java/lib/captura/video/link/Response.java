package lib.captura.video.link;

/**
 * The Response interface represents a response from an HTTP request.
 * It provides methods to retrieve the status code and the path of the downloaded video file.
 */
interface Response {

    /**
     * Retrieves the status code of the HTTP response.
     *
     * @return the status code of the HTTP response
     */
    int statusCode();

    /**
     * Retrieves the path of the downloaded video file.
     *
     * @return the path of the downloaded video file
     */
    String videoFilePath();

}
